const config = require('../config/index')
const sha1 = require('sha1')
const { getUserDataAsync, parseXMLAsync,formatMassge} = require("../utils/tool");
module.exports = ()=>{
    return async (req,res,next)=>{
        const {signature, echostr, timestamp, nonce} = req.query;
        const {token} = config;
        const arr = [timestamp, nonce, token]
        const arrSort = arr.sort()
        const str = arr.join('')
        const sha1Str = sha1(str)
        
        /*
            微信服务器发送两种类型的消息给开发服务器
            1.GET请求
                -验证服务器有效性
            2.POST请求
                -微信服务器会将用户发送的数据以POST请求的方式转发到开发者服务器上
        */
       if (req.method === 'GET') {
        if(sha1Str == signature) {
            res.send(echostr)
        } else {
            res.end('error')
        }
       } else if (req.method === 'POST') {
            // 微信服务器会将用户发送的数据以POST请求的方法发送到开发者服务器上
            // 验证消息来自于微信服务器
            if (sha1Str !== signature) {
                // 说明消息不是来自微信服务器
                res.end('error')
            } 
                // console.log(req.query)
            
            // 接受请求体中的数据，流式数据。
            const xmlData = await getUserDataAsync(req);
            const jsData = await parseXMLAsync(xmlData)
            console.log(jsData)
            let massage = formatMassge(jsData)
            console.log(massage)
            let content
            // 判断用户发送的消息是否是文本消息
            if(massage.MsgType === 'text') {
                if(massage.Content === '1') {
                    content = '监控和罚款'
                } else {
                    content = '您好'
                }
            }
            let replyMassage = `
            <xml>
                <ToUserName><![CDATA[${massage.FromUserName}]]></ToUserName>
                <FromUserName><![CDATA[${massage.ToUserName}]]></FromUserName>
                <CreateTime>${Date.now()}</CreateTime>
                <MsgType><![CDATA[text]]></MsgType>
                <Content><![CDATA[${content}]]></Content>
            </xml>
            `
            // 返回响应给微信服务器
            res.send(replyMassage)
       } else {
           res.end('error')
       }
    }
}