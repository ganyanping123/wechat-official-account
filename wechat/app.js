const express = require('express')
const sha1 = require('sha1')
const config = require('./config/index.js')
const auth = require('./wechat/auth.js')
// 引入wechat模块
const Wechat = require('./wechat/wechat')
// 引入config文件
const {url} = require('./config/index')
const app = express()
const port = 3000
// 配置模板资源目录
app.set('views', './views')
// 配置模板引擎
app.set('view engine', 'ejs')

const wechat = new Wechat()
// 页面路由
app.get('/search',async (req,res)=>{
    /**
     * 生成js-sdk使用的签名
     * 1.组合参与签名的四个参数，jsapi_ticket(临时票据), noncestr（随机字符）, timestamp（时间戳）, url（当前服务器地址）
     * 2.将其进行字典序排序，以&拼接在一起
     * 3.进行sha1加密，最终生产signature
     */
    // 获取随机字符串
    const noncestr = Math.random().split('.')[1];
    // 获取时间戳
    const timestamp = Date.now()
    // 获取票据
    const {ticket} = await wechat.fetchTicket();

    const arr = [
        `jsapi_ticket=${ticket}`,
        `noncestr=${noncestr}`,
        `timestamp=${timestamp}`,
        `url=${url}/search`
    ]
    const str = arr.sort().join('&');
    const signature = sha1(str)
    // 渲染页面，将渲染页面返回给用户
    res.render('search',{
        signature,
        noncestr,
        timestamp
    })
})
app.use(auth())

app.listen(port,()=>{
    console.log('服务器启动成功~~')
})