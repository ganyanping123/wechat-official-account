/*
工具函数包
*/
// 引入fs
const {writeFile, readFile} = require('fs')
// 引入path模块
const {resolve} = require('path')
const {parseString} = require('xml2js')
module.exports = {
    // 接受微信服务器发送的流式消息
    getUserDataAsync (req) {
        return new Promise((resolve,reject)=>{
            let xmlData = ''
            req.on('data',data=>{
                // 当流式数据传递过来时，会触发当前事件，会将数据注入到回调函数中
                // 读取的数据是buffer，需要将其转化成字符串。
                xmlData += data.toString()
            }).on('end',()=>{
                // 数据接受完毕时，会触发
                resolve(xmlData)
            })
        })
    },
    // 将接受的xml数据转化成js对象
    parseXMLAsync(xmlData) {
        return new Promise((resolve,reject)=>{
            parseString(xmlData, {trim: true}, function (err, data) {
                if(!err) {
                    resolve(data)
                } else {
                    reject('parseXMLAsync方法处理问题'+err)
                }
            });
        })
    },
    formatMassge(jsData){
        let massge = {}
        jsData = jsData.xml;
        if(typeof jsData === 'object') {
            for (let key in jsData) {
                let value = jsData[key]
                if(Array.isArray(value) && value.length >0) {
                    massge[key] = value[0]
                }
            }
        }
        return massge
    },
    writeFileAsync(data, fileName) {
        // 将对象转化成json字符串
        data = JSON.stringify(data)
        let filePath = resolve(__dirname, fileName)
        // 将accessToken保存一个文件
        return new Promise ((resolve,reject)=>{
            writeFile(filePath,data,err=>{
                if(!err) {
                    console.log('文件保存成功')
                    resolve()
                } else {
                    console.log('文件保存失败')
                    reject(err)
                }
            })
        })
    },
    readFileAsync(fileName) {
        let filePath = resolve(__dirname, fileName)
        return new Promise ((resolve,reject)=>{
            readFile(filePath,(err,data)=>{
                if(!err) {
                    console.log('文件读取成功')
                    data = JSON.parse(data)
                    resolve(data)
                } else {
                    console.log('文件读取失败')
                    reject(err)
                }
            })
        })
    }
}